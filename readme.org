#+TITLE: config | personal configuration / dotfiles

#+BEGIN_QUOTE
personal configuration used across all of my machines
#+END_QUOTE

* Usage

#+BEGIN_SRC sh
git clone --recurse-submodules https://gitlab.com/xeijin-dev/config ~/.config
#+END_SRC

note that config conforms to the =XDG_CONFIG= specification and =doom-emacs= and
associated config are supplied as submodules

* COMMENT Known Issues
* COMMENT Acknowledge
